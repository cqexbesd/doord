use tracing::{debug};

#[derive(Debug)]
pub struct IntercomMock {
}

impl IntercomMock {
    pub fn new<S>(_device: S, _pin: u32) -> Result<Self, gpio_cdev::Error>
    where
        S: std::convert::AsRef<std::path::Path>,
        S: std::fmt::Display,
    {

        Ok(Self {})
    }

	pub async fn buzz(&self) -> Result<(), gpio_cdev::Error> {

        debug!("setting pin to 1");

        Ok(())
	}

    pub async fn stop_buzz(&self) -> Result<(), gpio_cdev::Error> {
        
        debug!("setting pin to 0");
        Ok(())
    }
}

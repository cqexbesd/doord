use std::fs::File;
use std::io;
use std::io::{BufRead, Error};
use std::collections::HashMap;
use tracing::{error};

#[derive(Clone, Debug)]
pub struct PasswdFile {
    db: HashMap<String, String>
}

impl PasswdFile {
	pub fn new<S>(path: S) -> Result<Self, Error>
	where
		S: std::convert::AsRef<std::path::Path>,
		S: std::fmt::Display,
	{
        let mut rv = Self {
            db: HashMap::new()
        };

		let file = File::open(path)?;

		for (i, line) in io::BufReader::new(file).lines().enumerate() {
            if let Ok(rec) = line {
                let v: Vec<&str> = rec.split(':').collect();

                if v.len() < 2 {
                    error!("bad record on line {}", i);
                }
                rv.db.insert(v[0].to_string(), v[1].to_string());
            }
		}

        Ok(rv)
	}

    pub fn verify<'a>(&self, user: &'a String, pass: &String)
        -> Option<&'a String>
    {

        match self.db.get(user) {
            Some(real_pass) => {
                if pass == real_pass {
                    return Some(user);
                } else {
                    return None;
                }
            },
            None => None
        }
    }
}

use gpio_cdev::{Chip, LineHandle, LineRequestFlags};
use tracing::{debug, error};

#[derive(Debug)]
pub struct Intercom {
    switch: LineHandle,
}

impl Intercom {
    pub fn new<S>(device: S, pin: u32) -> Result<Self, gpio_cdev::Error>
    where
        S: std::convert::AsRef<std::path::Path>,
        S: std::fmt::Display,
    {

        let mut chip = match Chip::new(&device) {
            Ok(r) => {
                debug!("opened {}", device);
                r
            }
            Err(e) => {
                error!("failed to open {}: {}", &device, e);
                return Err(e);
            }
        };

        let line = match chip.get_line(pin) {
            Ok(r) => {
                debug!("found pin {}", pin);  
                r  
            },
            Err(e) => {
                error!("failed to find pin {}: {}", pin, e);
                return Err(e);
            }
        };

        let lh = match line.request(LineRequestFlags::OUTPUT, 0, "door") {
            Ok(lh) => lh,
            Err(e) => {
                error!("failed to set pin {} for output: {}", pin, e);
                return Err(e);
            }
        };

        Ok(Self {
            switch: lh,
        })
    }

	pub async fn buzz(&self) -> Result<(), gpio_cdev::Error> {

		return self.switch.set_value(1);
	}

    pub async fn stop_buzz(&self) -> Result<(), gpio_cdev::Error> {
        
        return self.switch.set_value(0);
    }
}

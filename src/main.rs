use axum::{
    extract::{Extension},
    http::{StatusCode, header},
    response::IntoResponse,
    routing::get,
    Router
};
use axum_auth::AuthBasic;
use axum_server::tls_rustls::RustlsConfig;
use clap::Parser;
use http::header::HeaderName;
use std::net::{
    Ipv6Addr,
    SocketAddr,
};
use std::process::exit;
use std::sync::{Arc,RwLock};
use tokio::time::{Duration, sleep};
use tracing::{debug, error, info};
use tracing_subscriber::{fmt, prelude::*, EnvFilter, filter::LevelFilter};

mod basic_auth;


#[cfg(feature = "mock")]
mod intercom_mock;
#[cfg(feature = "mock")]
use crate::intercom_mock::IntercomMock as Intercom;

#[cfg(not(feature = "mock"))]
mod intercom;
#[cfg(not(feature = "mock"))]
use crate::intercom::Intercom;

#[derive(Parser, Debug)]
#[clap(version = "1.0")]
struct CommandLineOpts {
    // port to listen on
    #[clap(short, long, default_value_t = 8443)]
    port: u16,

    // passwd DB
    #[clap(short('f'), long)]
    passwd_file: String,

    // public certificate
    #[clap(short('c'), long)]
    cert: String,

    // private key
    #[clap(short('k'), long)]
    key: String,
}

#[derive(Debug)]
struct Context {
    intercom: Intercom,

    serial: RwLock<u32>,

    passdb: basic_auth::PasswdFile,
}

#[tokio::main]
async fn main() {
    // parse command line arguments
    let opts = CommandLineOpts::parse();

    // enable logging
    tracing_subscriber::registry()
        .with(EnvFilter::builder()
              .with_default_directive(LevelFilter::INFO.into())
              .with_env_var("DOOR_LOG")
              .from_env_lossy())
        .with(fmt::layer().with_target(false))
        .init();

    // the GPIO handling thing
	let intercom_result = Intercom::new("/dev/gpiochip0", 16);
	if intercom_result.is_err() {
		exit(1);
	}

    let auth = match basic_auth::PasswdFile::new(&opts.passwd_file) {
        Ok(a) => a,
        Err(e) => {
            error!("failed to create authenticator:({}) {}",
                opts.passwd_file, e);
            exit(1);
        }
    };

    // the context passed to each handler
    let ctx = Context {
        intercom: intercom_result.unwrap(),
        serial: RwLock::new(0),
        passdb: auth,
    };

    // SSL config
    let tls_config = match RustlsConfig::from_pem_file(
            opts.cert,
            opts.key,
        ).await {
        Ok(r) => r,
        Err(e) => {
            error!("failed  to configure SSL: {}", e);
            exit(1);
        }
    };


    // create a buzzer object that has a gpio obejct and knows the right pin
    // then use extensions to pass it do subroutines
    let app = Router::new()
        .route("/door/unlock", get(unlock))
        .route("/door/cancel", get(cancel))
        .layer(Extension(Arc::new(ctx)));

    let addr = SocketAddr::new(
        std::net::IpAddr::V6(Ipv6Addr::UNSPECIFIED), opts.port
    );
    info!("listening on {}", addr);

    // launch the server
    match axum_server::bind_rustls(addr, tls_config)
        .serve(app.into_make_service())
        .await {
            Ok(_) => {
                info!("exiting succesfully");
            },
            Err(e) => {
                error!("server failed: {}", e);
            }
    };
}

async fn unlock(
    Extension(ctx): Extension<Arc<Context>>,
    auth_info: Option<AuthBasic>
) -> impl IntoResponse {

    let ctx_clone = ctx.clone();

    let (code, hdr_name, hdr_value, msg) = check_auth(&ctx.passdb, auth_info);
    if code != StatusCode::OK {
        return (
            code,
            [
                ( hdr_name, hdr_value.to_string() )
            ],
            msg.to_string());
    }

    // increment serial
    let orig_serial: u32;
    {
        let mut curr_serial = ctx_clone.serial.write().unwrap();
        *curr_serial += 1;
        orig_serial = *curr_serial;
    }
    debug!("setting serial to {} before buzz", orig_serial);

    match ctx_clone.intercom.buzz().await {
        Ok(_) => {
			info!("buzzing");
		},
        Err(e) => {
            error!("failed to buzz: {}", e);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                [
                    ( hdr_name, hdr_value.to_string() ),
                ],
                "Failed to buzz. I hope you have keys.".to_string()
            );
        }
    }

	tokio::spawn(async move {
		sleep(Duration::from_secs(5)).await;

        // check if someone has buzzed again
        let curr_serial = *(ctx_clone.serial.read().unwrap());

        if curr_serial != orig_serial {
            // someone has buzzed again so we no longer want to cancel
            debug!("cancelling cancel as serial is now {}", curr_serial);
            return;
        }
        
		match ctx_clone.intercom.stop_buzz().await {
			Ok(_) => {
				info!("stopped buzzing after timer");
			},
			Err(e) => {
				error!("failed to stop buzzing: {}", e);
			}
		};
	});

    return (
        code,
        [
            ( hdr_name, hdr_value.to_string() )
        ],
        "Buzzing now".to_string()
    );
}

async fn cancel(
    Extension(ctx): Extension<Arc<Context>>,
    auth_info: Option<AuthBasic>
) -> impl IntoResponse {

    let (code, hdr_name, hdr_value, msg) = check_auth(&ctx.passdb, auth_info);
    if code != StatusCode::OK {
        return (
            code,
            [
                ( hdr_name, hdr_value.to_string() )
            ],
            msg.to_string());
    }

    match ctx.intercom.stop_buzz().await {
		Ok(_) => {
			info!("stopped buzzing");
		},
        Err(e) => {  
            error!("failed to stop the buzz: {}", e);                  
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                [
                    ( hdr_name, hdr_value.to_string() ),
                ],
                "Failed to stop the buzz. I hope you can unplug something!".to_string()
            );
        }
    }

    return (
        StatusCode::OK,
        [
            ( hdr_name, hdr_value.to_string() ),
        ],
        "Stopped buzzing".to_string()
    );
}

fn check_auth(db: &basic_auth::PasswdFile, auth_info: Option<AuthBasic>)
    -> (StatusCode, HeaderName, &str, &str) {

    // check if we have a username and extract it and the password
    if let Some(auth_info_unwrapped) = auth_info {
        let (user, pass) = auth_info_unwrapped.0;
        let pass = pass.unwrap_or("".to_string());

        // now check the DB
        if let Some(name) = db.verify(&user, &pass) {
            info!("authenticated user {}", name);
            return (
                StatusCode::OK,
                header::WWW_AUTHENTICATE, "Basic realm=\"Front Door\"",
                "Authentication Successful"
                );
        } else {
            info!("user {} failed auth", user);
        }
    }

    return (
        StatusCode::UNAUTHORIZED,
        header::WWW_AUTHENTICATE, "Basic realm=\"Front Door\"",
        "Authentication Failed"
        );
}
